import subprocess, os, sys
import httplib2
import base64
import json
import tempfile
import dircache
import datetime
import threading

basecamp_project_id = '1979461'
username = 'gkatatikarn@wsgc.com'
password = '1Bgunner'
user_agent = "WEST ELM BRANDING HOTLIST (gungkat@gmail.com)"
headers = {	'User-Agent' : user_agent }
finished_date_desc = u'\u2713'
tbd_desc = 'TBD'
na_desc = u'\u2012'

copy_due_desc = ["copy due", "final copy/layout"]
copy_rnd1_desc = ["copy rnd 1", "copy r1"]
proof_eta_desc = ["proof eta", "proof", "test samples status", "proof status", "specs needed",]
finalize_artwork_desc = ["finalize artwork", "final a/w"]
artwork_list_desc = ["a/w r1", "artwork r1", "A/W R1", "R1 a/w", "R1 A/W", "release list", "sent", "sent to vendor", "specs & as400", "release artwork to vendor", "release a/w to vendor", "release", "to vendor"]
approvals_routing_desc = ["approvals/routing"]

name_map = {
	'Gungsadawn':'Gung',
	'Mandy':'Mandy',
	'Monica':'Monica',
	'Christopher':'Chris',
	'lindsay':'Lindsay',
	'Arnau':'Arnau',
	'Assortment':'Assort.'
}

def check_todo_title( title, description ):
	if isinstance(description, list):
		for x in description:
			if title.lower().startswith(x):
				return True
		return False

	else:
		return title.lower().startswith(description)

print(user_agent)

temp_dir = os.path.join( tempfile.gettempdir(), 'basecamp_report' )
connection = httplib2.Http( temp_dir )
connection.add_credentials(username, password)

def validate_date(date):
	return tbd_desc if date is None else date

def read_basecamp_file(file, dump_content=False):
	resp, content = connection.request(file, 'GET', headers=headers)
	if dump_content:
		print(content)
	return json.loads(content)

class Project(object):
	def __init__(self, name):
		self.name = name
		self.current_task_owner = None
		self.current_project_task = None
		self.task_due_at = None
		self.release_artwork_date = None
		self.finalize_artwork_date = None
		self.copy_due_date = None
		self.copy_rnd1_date = None
		self.proof_eta_date = None
		self.finalize_artwork_owner = None
		self.assortment_name = None
		self.artwork_list_date = None
		self.is_strategy = False


class ReadProjectThread(threading.Thread):

	def __init__(self, name, url):
		threading.Thread.__init__(self)
		self.name = name
		self.url = url
		self.connection = httplib2.Http( temp_dir )
		self.connection.add_credentials(username, password)
		self.projects = []

	

	def read_basecamp_file(self, file, debug_dump=False):
		resp, content = self.connection.request(file, 'GET', headers=headers)
		if debug_dump:
			print (content)
		return json.loads(content)		

	def run(self):
		project = self.read_basecamp_file(self.url)
		
		"""
		if self.name == 'P [FL14 MKT] Novelty Hangers':
			print self.url
			{
				"id":4291227,
				"name":"P [FL14 MKT] Novelty Hangers",
				"description":"",
				"archived":false,
				"is_client_project":false,
				"created_at":"2013-11-08T14:07:52.000-05:00",
				"updated_at":"2014-01-05T17:02:34.000-05:00",
				"last_event_at":"2014-01-05T17:02:34.000-05:00",
				"starred":false,
				"creator":{
					"id":3358544,
					"name":"Mandy Braatz",
					"avatar_url":"http://dge9rmgqjs8m1.cloudfront.net/builtin/default_avatar_v1_4/avatar.gif?r=3"
				},
				"accesses":{
					"count":10,
					"updated_at":"2013-11-27T15:51:47.000-05:00",
					"url":"https://basecamp.com/1979461/api/v1/projects/4291227-p-fl14-mkt/accesses.json"
				},
				"attachments":{
					"count":5,
					"updated_at":"2013-12-02T10:48:07.000-05:00",
					"url":"https://basecamp.com/1979461/api/v1/projects/4291227-p-fl14-mkt/attachments.json"
				},
				"calendar_events":{
					"count":0,
					"updated_at":null,
					"url":"https://basecamp.com/1979461/api/v1/projects/4291227-p-fl14-mkt/calendar_events.json"
				},
				"documents":{
					"count":0,
					"updated_at":null,
					"url":"https://basecamp.com/1979461/api/v1/projects/4291227-p-fl14-mkt/documents.json"
				},
				"forwards":{
					"count":0,
					"updated_at":null,
					"url":"https://basecamp.com/1979461/api/v1/projects/4291227-p-fl14-mkt/forwards.json"
				},
				"topics":{
					"count":2,
					"updated_at":"2013-12-05T13:35:00.000-05:00",
					"url":"https://basecamp.com/1979461/api/v1/projects/4291227-p-fl14-mkt/topics.json"
				},
				"todolists":{
					"remaining_count":1,
					"completed_count":4,
					"updated_at":"2014-01-05T17:02:34.000-05:00",
					"url":"https://basecamp.com/1979461/api/v1/projects/4291227-p-fl14-mkt/todolists.json"
				}
			}"""

		todolists_url = project['todolists']['url']
		todo_list_descriptions = self.read_basecamp_file(todolists_url)

		found_approvals_routing = False
		found_execution = False

		old_project = Project(self.name)
		for todo_list_description in todo_list_descriptions:
			todo_list = None

			if old_project.current_project_task == None and not todo_list_description['completed']:
				todo_list = self.read_basecamp_file(todo_list_description['url'])

				try:
					first_remaining = todo_list['todos']['remaining'][0]
				except:
					first_remaining = None
				
				try:
					old_project.current_task_owner = first_remaining['assignee']['name']
				except:
					old_project.current_task_owner = ''

				try:
					old_project.current_project_task = first_remaining['content']
				except:
					pass

				try:
					old_project.task_due_at = first_remaining['due_at']
				except:
					old_project.task_due_at = ''
		
			if todo_list_description['name'].lower() == 'execution':

				found_execution = True
				
				todo_list = todo_list if todo_list != None else self.read_basecamp_file(todo_list_description['url'])
	
				#extract both dates and designer name from to do list
				for remaining in todo_list['todos']['remaining']:

										
					if check_todo_title( remaining['content'], copy_due_desc ):
						old_project.copy_due_date = validate_date( remaining['due_at'] )
						
					if check_todo_title( remaining['content'], copy_rnd1_desc ):
						old_project.copy_rnd1_date = validate_date( remaining['due_at'] )

					if check_todo_title( remaining['content'], artwork_list_desc ):
						old_project.artwork_list_date = validate_date( remaining['due_at'] )

					if check_todo_title( remaining['content'], proof_eta_desc):
						old_project.proof_eta_date = validate_date( remaining['due_at'] )

					if check_todo_title( remaining['content'], finalize_artwork_desc ):
						old_project.finalize_artwork_date = validate_date( remaining['due_at'] )
						
						try:
							old_project.finalize_artwork_owner = remaining['assignee']['name']
						except:
							pass
						
				if None in [ old_project.finalize_artwork_date, old_project.copy_due_date , old_project.copy_rnd1_date ]:
					for completed in todo_list['todos']['completed']:
						if old_project.finalize_artwork_date == None and check_todo_title( completed['content'], finalize_artwork_desc ):
							old_project.finalize_artwork_date = finished_date_desc

							try:
								old_project.finalize_artwork_owner = completed['assignee']['name']
							except:
								pass
							
						if old_project.copy_rnd1_date == None and check_todo_title( completed['content'], copy_rnd1_desc ):
							old_project.copy_rnd1_date = finished_date_desc

						if old_project.proof_eta_date == None and check_todo_title( completed['content'], proof_eta_desc):
							old_project.proof_eta_date = finished_date_desc

						if old_project.copy_due_date == None and check_todo_title( completed['content'], copy_due_desc ):
							old_project.copy_due_date = finished_date_desc

						if old_project.copy_due_date == None and check_todo_title( completed['content'], artwork_list_desc ):
							old_project.artwork_list_date = finished_date_desc


			if check_todo_title( todo_list_description['name'], approvals_routing_desc ):

				found_approvals_routing = True

				todo_list = todo_list if todo_list != None else self.read_basecamp_file(todo_list_description['url'])

				for remaining in todo_list['todos']['remaining']:
					if check_todo_title( remaining['content'], artwork_list_desc ):
						old_project.artwork_list_date = remaining['due_at']

				if old_project.release_artwork_date == None:
					for remaining in todo_list['todos']['completed']:
						if check_todo_title( remaining['content'], artwork_list_desc ):
							old_project.artwork_list_date = finished_date_desc

				for remaining in todo_list['todos']['remaining']:
					if check_todo_title( remaining['content'], proof_eta_desc ):
						old_project.proof_eta_date = remaining['due_at']

				if old_project.release_artwork_date == None:
					for remaining in todo_list['todos']['completed']:
						if check_todo_title( remaining['content'], proof_eta_desc ):
							old_project.proof_eta_date = finished_date_desc

			if check_todo_title( todo_list_description['name'], 'strategy' ):
				old_project.is_strategy = True


		if (found_execution and found_approvals_routing) or old_project.is_strategy:

			if old_project.finalize_artwork_date == None or old_project.release_artwork_date == None or old_project.copy_due_date == None:
				completed_todo_list_descriptions = self.read_basecamp_file(todolists_url[:len(todolists_url)-5] + '/completed.json')

				for todo_list_description in completed_todo_list_descriptions:
					if todo_list_description['name'].lower() == 'execution':
						todo_list = self.read_basecamp_file(todo_list_description['url'])
					
						for completed in todo_list['todos']['completed']:
							if old_project.finalize_artwork_date == None and check_todo_title( completed['content'], finalize_artwork_desc ):
								old_project.finalize_artwork_date = finished_date_desc
							
								try:
									old_project.finalize_artwork_owner = completed['assignee']['name']
								except:
									pass

							if old_project.copy_rnd1_date == None and check_todo_title( completed['content'], copy_due_desc ):
								old_project.copy_due_date = finished_date_desc
						
							if old_project.proof_eta_date == None and check_todo_title( completed['content'], proof_eta_desc):
								old_project.proof_eta_date = finished_date_desc
						
							if old_project.copy_due_date == None and check_todo_title( completed['content'], copy_due_desc ):
								old_project.copy_due_date = finished_date_desc

					elif check_todo_title( todo_list_description['name'], approvals_routing_desc ):
						todo_list = self.read_basecamp_file(todo_list_description['url'])
					
						for completed in todo_list['todos']['completed']:
							if old_project.artwork_list_date == None and check_todo_title( completed['content'], artwork_list_desc ):
								old_project.artwork_list_date = finished_date_desc
						for completed in todo_list['todos']['completed']:
							if old_project.proof_eta_date == None and check_todo_title( completed['content'], proof_eta_desc ):
								old_project.proof_eta_date = finished_date_desc
					
			if old_project.current_project_task == None:
				old_project.current_project_task = ''

			if old_project.finalize_artwork_owner == None:
				old_project.finalize_artwork_owner = tbd_desc

			self.projects.append(old_project)



		else:
			# new collection-based format
			# print "New collection based format in " + self.name

			for todo_list_description in todo_list_descriptions:
				p = Project(self.name + ': ' + todo_list_description['name'])
				todo_list = self.read_basecamp_file(todo_list_description['url'])

				for remaining_todo in todo_list['todos']['remaining']:

					if p.current_project_task == None:
						p.current_project_task = remaining_todo['content']

						try:
							p.current_task_owner = remaining_todo['assignee']['name']
						except:
							pass
						if p.current_task_owner == None:
							p.current_task_owner = tbd_desc

					if check_todo_title( remaining_todo['content'], copy_rnd1_desc ):
						p.copy_rnd1_date = remaining_todo['due_on']

					elif check_todo_title( remaining_todo['content'], finalize_artwork_desc ):
						p.finalize_artwork_date = validate_date( remaining_todo['due_on'] )

						#finalize_artwork_item = self.read_basecamp_file(remaining_todo['url'], True)
						try:
							p.finalize_artwork_owner = remaining_todo['assignee']['name']
						except:
							pass

						if p.finalize_artwork_owner == None:
							p.finalize_artwork_owner = tbd_desc

					elif check_todo_title( remaining_todo['content'], copy_due_desc ):
						p.copy_due_date = validate_date( remaining_todo['due_on'] )

					elif check_todo_title( remaining_todo['content'], artwork_list_desc ):
						p.artwork_list_date = validate_date( remaining_todo['due_on'] )

					elif check_todo_title( remaining_todo['content'], proof_eta_desc ):
						p.proof_eta_date = validate_date( remaining_todo['due_on'] )


				for completed_todo in todo_list['todos']['completed']:

					if check_todo_title( completed_todo['content'], copy_rnd1_desc ):
						p.copy_rnd1_date = finished_date_desc

					elif check_todo_title( completed_todo['content'], finalize_artwork_desc ):
						p.finalize_artwork_date = finished_date_desc

						#finalize_artwork_item = self.read_basecamp_file(completed_todo['url'], True)
						try:
							p.finalize_artwork_owner = completed_todo['assignee']['name']
						except:
							pass

						if p.finalize_artwork_owner == None:
							p.finalize_artwork_owner = tbd_desc

					elif check_todo_title( completed_todo['content'], copy_due_desc ):
						p.copy_due_date = finished_date_desc

					elif check_todo_title( completed_todo['content'], artwork_list_desc ):
						p.artwork_list_date = finished_date_desc

					elif check_todo_title( completed_todo['content'], proof_eta_desc ):
						p.proof_eta_date = finished_date_desc


				self.projects.append(p)

tasks = []
projects = []
strategies = []
project_descriptions = read_basecamp_file('https://basecamp.com/%s/api/v1/projects.json' % basecamp_project_id)

print('Hotlist in Progress. Lets make it another great week!')
for project_desc in project_descriptions:

	if project_desc['name'].startswith('P '):

		t = ReadProjectThread(project_desc['name'], project_desc['url'])
		tasks.append(t)
		t.start()

# sort the projects by the task due date

for t in tasks:
	t.join()
	
	def na_if_none(x):
		return na_desc if x is None else x

	for project in t.projects:
		if not project.is_strategy:
			projects.append({
					'name' : project.name,
					'status' : project.current_project_task,
					'designer' : project.finalize_artwork_owner,
					'release artwork' : na_if_none( project.release_artwork_date ),
					'artwork list': na_if_none ( project.artwork_list_date ),
					'copy rnd1' : na_if_none( project.copy_rnd1_date ),
					'proof eta' : na_if_none( project.proof_eta_date ),
					'copy due' : na_if_none( project.copy_due_date ),
					'finalize artwork' : na_if_none( project.finalize_artwork_date )
				})
		else:
			strategies.append({
					'name' : project.name,
					'status' : project.current_project_task,
					'owner': project.current_task_owner,
					'task due': project.task_due_at						  
				})

NA_KEY = 999999999999999999999
FINISHED_KEY = NA_KEY-1
TBD_KEY = FINISHED_KEY-1

def get_date_key(date):

	if date == None:
		return NA_KEY

	elif date == tbd_desc:
		return TBD_KEY

	elif date == finished_date_desc:
		return FINISHED_KEY

	year, month, day = date.split('-')

	return int(year) * (32*12) + 32 * int(month) + int(day)	


def project_task_to_key(x):

	result_key = NA_KEY
	for column_text in ['release artwork', 'proof eta', 'artwork list', 'copy rnd1', 'copy due', 'finalize artwork']:
		try:
			result_key = min(result_key, get_date_key(x[column_text]))
		except:
			pass

	return result_key

def strategy_task_to_key(x):
	sort_date = x['task due']
	
	if sort_date:
		year, month, day = sort_date.split('-')
		
		return int(year) * (32*12) + 32 * int(month) + int(day)
	else:
		return 999999999999999999999

projects.sort(key = project_task_to_key)
strategies.sort(key = strategy_task_to_key)

xls_file = tempfile.NamedTemporaryFile(mode='w+b', suffix='.xls', delete=False)
xls_file.close()

import xlwt

w = xlwt.Workbook()
ws = w.add_sheet('Report')

title_style = xlwt.XFStyle()
title_style.font.height = int(title_style.font.height * 1.2)
title_style.pattern.pattern = xlwt.Pattern.SOLID_PATTERN
title_style.pattern.pattern_fore_colour = 0x16

t = datetime.date.today()

ws.write(0, 0, 'BRANDING & DESIGN HOTLIST %s/%s/%s' % (t.month, t.day, t.year), title_style)
ws.row(0).set_style(title_style)

header_style = xlwt.XFStyle()
header_style.borders.bottom = True
header_style.borders.top = True
header_style.pattern.pattern = xlwt.Pattern.SOLID_PATTERN
header_style.pattern.pattern_fore_colour = 0x29

row = 1

def reformat_name(x):
		single_names=x.split(' ')

		try:
			return name_map[single_names[0]]
		except:
			return single_names[0]


def reformat_date(x):
	try:
		year, month, day = x.split('-')
		
		return month + '/' + day 
	except:
		return x


date_style = xlwt.XFStyle()
date_style.num_format_str  = 'M/D'

number_style = xlwt.XFStyle()
number_style.num_format_str = '0'

ws.row(row).set_style(header_style)
ws.write(row, 1, 'STRATEGY', header_style)
ws.write(row, 2, 'STATUS', header_style)

row += 1

for x in strategies:
	ws.write(row, 1, str(x['name']))
	ws.write(row, 2, str(x['status']))
	row += 1
					

ws.row(row).set_style(header_style)
ws.write(row, 0, '', header_style)
ws.write(row, 1, 'PROGRAM', header_style)
ws.write(row, 2, 'LEAD', header_style)
ws.write(row, 3, 'COPY', header_style)
ws.write(row, 4, 'R1 A/W', header_style)
ws.write(row, 5, 'A/W', header_style)


row += 1
			 
			 
start_row = row
for x in projects:
	ws.write(row, 0, row-start_row+1, number_style)
	ws.write(row, 1, str(x['status']))
	ws.write(row, 2, reformat_name(str(x['designer'])))
	ws.write(row, 3, reformat_date(x['copy rnd1']), date_style)
	ws.write(row, 4, reformat_date(x['artwork list']), date_style)
	ws.write(row, 5, reformat_date(x['finalize artwork']), date_style)


	
	row += 1

default_width = 0x0B92
ws.col(0).set_width(int(default_width*0.3))
ws.col(1).set_width(int(default_width*11.6))
ws.col(2).set_width(int(default_width*0.6))
ws.col(3).set_width(int(default_width*0.5))
ws.col(4).set_width(int(default_width*0.5))
ws.col(5).set_width(int(default_width*0.5))



w.save(xls_file.name)

if sys.platform.startswith('darwin'):
	subprocess.call(('open', xls_file.name))
elif os.name == 'nt':
	os.startfile(xls_file.name)
elif os.name == 'posix':
	subprocess.call(('xdg-open', xls_file.name))
